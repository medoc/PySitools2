#    SITools2 client for Python
#    Copyright (C) 2020 - Institut d'Astrophysique Spatiale
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
__author__ = ["Pablo ALINGERY, Eric BUCHLIN, Stephane CAMINADE, Jean-Christophe MALAPERT, Elie SOUBRIE, Nima TRAORE"]
__date__ = "$30 Dec 2024 14:47:00$"
__version__ = "1.0.2"

from sitools2.clients.euvsyn_client_medoc import EuvsynClientMedoc
from sitools2.clients.gaia_client_medoc import GaiaClientMedoc
from sitools2.clients.sdo_client_medoc import SdoClientMedoc
from sitools2.clients.soho_client_medoc import SohoClientMedoc
from sitools2.clients.stereo_client_medoc import StereoClientMedoc

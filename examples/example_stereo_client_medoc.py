#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module provides some examples of use on how to interrogate
Sitools2 with the stereo_client_medoc.py in IDOC/MEDOC at IAS

"""

__author__ = "Nima TRAORE"

# Import the class StereoClientMedoc in order to be instantiated
from sitools2 import StereoClientMedoc

# Import python datetime in order to create date objects
from datetime import datetime

# Define a date range for the request (date 1 < date 2)
d1 = datetime(2019, 4, 4, 0, 0, 0)
d2 = datetime(2019, 4, 4, 1, 0, 0)

# Note in following examples that args, inside both the search() and
# get() methods, can be written either in upper case or in lower case.

# ==========
# Example 1:
# ==========

# Instantiate the class StereoClientMedoc
stereo = StereoClientMedoc()

# Call the search() method to get STEREO data list from the web server
# by providing a date range (mandatory) and optionally you can provide
# a number corresponding to the maximum number results to be returned
# (nb_res_max=10 means limit the output results to 10 maximum)
stereo_data_list = stereo.search(dates=[d1, d2], nb_res_max=10)

# Call the get() method to download the previous search data (by
# assigning target_dir='results', data will be downloaded in this
# directory). If the directory 'results' does not exist, it will be
# created in the current directory.
stereo.get(data_list=stereo_data_list, target_dir="results", download_type="TAR")

# ==========
# Example 2:
# ==========

# Instantiate the class StereoClientMedoc
stereo_obj = StereoClientMedoc()

# Call the search() method to get STEREO data list from the web server
# by providing a date range (mandatory), filter the search by getting
# specific detectors (
# detectors=['SECCHI/COR1', 'SECCHI/EUVI', 'SECCHI/HI2']) and limit the
# maximum number results to be returned to 18 (nb_res_max=18)
stereo_data = stereo_obj.search(
    dates=[d1, d2],
    detectors=["SECCHI/COR1", "SECCHI/EUVI", "SECCHI/HI2"],
    nb_res_max=18,
)

# Call the get() method to download the previous search data (here
# the argument target_dir is not provided, then by default data will be
# downloaded in the current directory)
stereo_obj.get(data_list=stereo_data, download_type="ZIP")

# ==========
# Example 3:
# ==========

# Instantiate the class StereoClientMedoc
my_obj = StereoClientMedoc()

# Call the search() method to get STEREO data list from the web server
# by providing a date range (mandatory), in this case the argument
# nb_res_max is not provided, then by default all the found results
# will be returned
data = my_obj.search(dates=[d1, d2])

# Call the get() method to download the previous search data (by
# assigning target_dir='stereo_results', data will be downloaded in this
# directory). If the directory 'stereo_results' does not exist, it will
# be created in the current directory.
my_obj.get(data_list=data, target_dir="stereo_results", download_type="TAR")

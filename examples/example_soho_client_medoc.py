#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module provides some examples of use on how to interrogate
Sitools2 with the soho_client_medoc.py in IDOC/MEDOC at IAS

"""

__author__ = "Nima TRAORE"

# Import the class SohoClientMedoc in order to be instantiated
from sitools2 import SohoClientMedoc

# Import python datetime in order to create date objects
from datetime import datetime

# Define a date range for the request (date 1 < date 2)
d1 = datetime(2015, 3, 31, 0, 0, 0)
d2 = datetime(2015, 4, 1, 0, 0, 0)

# Note in following examples that args, inside both the search() and
# get() methods, can be written either in upper case or in lower case.

# ==========
# Example 1:
# ==========

# Instantiate the class SohoClientMedoc
soho = SohoClientMedoc()

# Call the search() method to get SOHO data list from the web server
# by providing a date range (mandatory) and optionally you can provide
# a number corresponding to the maximum number results to be returned
# (nb_res_max=10 means limit the output results to 10 maximum)
soho_data_list = soho.search(dates=[d1, d2], nb_res_max=10)

# Call the get() method to download the previous search data (by
# assigning target_dir='results', data will be downloaded in this
# directory). If the directory 'results' does not exist, it will be
# created in the current directory.
soho.get(data_list=soho_data_list, target_dir="results", download_type="TAR")

# ==========
# Example 2:
# ==========

# Instantiate the class SohoClientMedoc
soho_obj = SohoClientMedoc()

# Call the search() method to get SOHO data list from the web server
# by providing a date range (mandatory), filter the search by getting
# specific detectors (detectors=['CELIA/CTOF', 'LASCO/C2', 'VIRGO/LOI'])
# and limit the maximum number results to be returned to 15
# (nb_res_max=15)
soho_data = soho_obj.search(
    dates=[d1, d2], detectors=["CELIAS/CTOF", "LASCO/C2", "VIRGO/LOI"], nb_res_max=15
)

# Call the get() method to download the previous search data (here
# the argument target_dir is not provided, then by default data will be
# downloaded in the current directory)
soho_obj.get(data_list=soho_data, download_type="ZIP")

# ==========
# Example 3:
# ==========

# Instantiate the class SohoClientMedoc
my_obj = SohoClientMedoc()

# Call the search() method to get SOHO data list from the web server
# by providing a date range (mandatory), in this case the argument
# nb_res_max is not provided, then by default all the found results
# will be returned
data = my_obj.search(dates=[d1, d2])

# Call the get() method to download the previous search data (by
# assigning target_dir='soho_results', data will be downloaded in this
# directory). If the directory 'soho_results' does not exist, it will
# be created in the current directory.
my_obj.get(data_list=data, target_dir="soho_results", download_type="TAR")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module provides some examples of use on how to interrogate
Sitools2 with the gaia_client_medoc.py in IDOC/MEDOC at IAS

"""

__author__ = "Nima TRAORE"

# Import the class GaiaClientMedoc in order to be instantiated
from sitools2 import GaiaClientMedoc

# Import python datetime in order to create date objects
from datetime import datetime

# Define a date range for the request (date 1 < date 2)
d1 = datetime(2019, 9, 2, 0, 0, 0)
d2 = datetime(2019, 9, 3, 0, 0, 0)

# Note in following examples that args, inside both the search() and
# get() methods, can be written either in upper case or in lower case.

# ==========
# Example 1:
# ==========

# Instantiate the class GaiaClientMedoc
gaia = GaiaClientMedoc()

# Call the search() method to get the data list from the web server
# by providing a date range (mandatory) and optionally you can provide
# a number corresponding to the maximum number results to be returned
# (nb_res_max=10 means limit the output results to 10 maximum)
gaia_data_list = gaia.search(dates=[d1, d2], nb_res_max=10)

# Call the get() method to download the previous search data (by
# assigning target_dir='gaia_results', data will be downloaded in this
# directory). If the directory 'gaia_results' does not exist, it will be
# created in the current directory.
gaia.get(data_list=gaia_data_list, target_dir="gaia_results", download_type="TAR")

# ==========
# Example 2:
# ==========

# Instantiate the class GaiaClientMedoc
ga = GaiaClientMedoc()

# Call the search() method to get the data list from the web server
# by providing a date range (mandatory) and optionally you can provide
# a number corresponding to the maximum number results to be outputted
# (nb_res_max=4 means limit the output results to 4 maximum)
search_data = ga.search(dates=[d1, d2], nb_res_max=4)

# Call the get() method to download the previous search data (here
# the argument target_dir is not provided, then by default data will be
# downloaded in the current directory)
ga.get(data_list=search_data, download_type="ZIP")

# Call the get_item_file() method to download the previous search data
# without any compressed mode (getting files individually) and the
# optional argument target_dir='gaia_tmp_dir' will put the downloading
# results into the 'gaia_tmp_dir' directory (created if does not exist)
ga.get_item_file(data_list=search_data, target_dir="gaia_tmp_dir")

# The previous result can be found by calling the get() method without
# specifying download_type in the method argument
ga.get(data_list=search_data, target_dir="gaia_tmp_dir")

# ==========
# Example 3:
# ==========

# Instantiate the class GaiaClientMedoc
my_obj = GaiaClientMedoc()

# Call the search() method to get the data list from the web server
# by providing a date range (mandatory), in this case the argument
# nb_res_max is not provided, then by default all the found results
# will be returned
data = my_obj.search(dates=[d1, d2])

# Call the get() method to download the previous search data (by
# assigning target_dir='results', data will be downloaded in this
# directory). If the directory 'results' does not exist, it will be
# created in the current directory.
my_obj.get(data_list=data, target_dir="results", download_type="TAR")

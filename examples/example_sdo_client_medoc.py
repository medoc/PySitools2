#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module provides some examples about how to query the
Sitools2 API for SDO data at IDOC/MEDOC at IAS
"""

__authors__ = ["Eric Buchlin", "Nima TRAORE"]

# Imports
from astropy.io import fits
from astropy.time import Time, TimeDelta
from astropy.wcs import WCS
from datetime import datetime
from matplotlib import pyplot as plt
from sunpy.map import Map

from sitools2 import SdoClientMedoc


# Define a date range for the request (date 1 < date 2)
d1 = datetime(2020, 1, 15, 0, 0, 0)
d2 = datetime(2020, 1, 15, 2, 0, 0)


# ===========================================
# Example 1: search AIA data and get metadata
# ===========================================

# Instantiate the SdoClientMedoc class
aia = SdoClientMedoc()

# Search data (all wavelengths)
aia_data_list = aia.search(dates=[d1, d2], nb_res_max=5)

# Same, setting series and cadence explicitly (aia.lev1 and 1m are the default values)
aia_data_list = aia.search(
    dates=[d1, d2], series="aia.lev1", cadence=["1m"], nb_res_max=5
)

# It is also possible to select only a few wavelengths
aia_data_list = aia.search(dates=[d1, d2], waves=["94", "304", "1700"], nb_res_max=5)

# Download the files in the list.
# The default target directory is the current directory.
# The target directory is created if it does not exist already.
aia.get(data_list=aia_data_list, target_dir="data")
# The files can be downloaded as a single TAR file with download_type="TAR"

# Get the corresponding metadata
keyword_list = ["recnum", "sunum", "date__obs", "quality", "cdelt1", "cdelt2", "crval1"]
metadata_list = aia.sdo_metadata_search(data_list=aia_data_list, keywords=keyword_list)


# ======================================================
# Example 2: search HMI m_720s, get and use WCS metadata
# ======================================================

# Instantiate the SdoClientMedoc class
# Note: at the moment you cannot use the same instance for
# both AIA and HMI queries
hmi = SdoClientMedoc()

# Search data, with the default 12min cadence
hmi_data_list = hmi.search(dates=[d1, d2], series="hmi.m_720s", nb_res_max=20)

# Same, setting cadence and wavelength explicitly
hmi_data_list = hmi.search(
    dates=[d1, d2], series="hmi.m_720s", cadence=["12m"], waves=["6173"], nb_res_max=20
)
# Download the first file in the list
hmi_data_list = [hmi_data_list[0]]
hmi.get(data_list=hmi_data_list, target_dir="data", download_type="TAR")
# Then use the shell or Python tarfile library to un-tar the downloaded file

# The WCS metadata, which are required to work with coordinates, are not
# included in the HMI FITS files (and might not be up-to-date in the AIA
# files), but we can get them from the database, as we will see now


def sdo_time_to_utc(sdo_time):
    """
    Convert SDO time from the database (seconds since 1977-01-01T00:00:00TAI)
    to UTC datetime. Courtesy Gabriel Pelouze.
    """
    t_ref = Time("1977-01-01T00:00:00", scale="tai")
    t_tai = t_ref + TimeDelta(sdo_time, format="sec", scale="tai")
    return t_tai.utc


# Get WCS metadata and Python object
wcs_meta = [
    "date__obs",
    "crpix1",
    "crpix2",
    "cdelt1",
    "cdelt2",
    "crota2",
    "dsun_obs",
    "crln_obs",
    "crlt_obs",
    "rsun_obs",
]
metadata = hmi.sdo_metadata_search(data_list=hmi_data_list, keywords=wcs_meta)[0]
metadata["date-beg"] = sdo_time_to_utc(metadata["date__obs"]).isot
metadata.pop("date__obs")
metadata.update(
    {
        "cunit1": "arcsec",
        "cunit2": "arcsec",
        "ctype1": "HPLN-TAN",
        "ctype2": "HPLT-TAN",
        "rsun_ref": 696000000.0,
        "naxis1": 4096,
        "naxis2": 4096,
    }
)
wcs = WCS(metadata)

# Get sunpy map for this file and metadata
with fits.open(
    "data/hmi.m_720s.6173.652100.2020-01-15T00:10:29.700Z.magnetogram.fits"
) as hdu_list:
    data = hdu_list[1].data
    m = Map(data, wcs)
m.plot(vmin=-100, vmax=100)
plt.show()

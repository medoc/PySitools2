**Architects and Developers MUST be aware of the context of the project before they start anything.**

*This document provides a minimal template that MUST be filled in before any IDOC software project is started. It MUST be made available and easily findable to all stakeholders. It MUST be present alongside the software source code.*

# Project overview

* Title: MEDOC PySitools2
* Brief description: A generic python Sitools2 client with IDOC/MEDOC clients.
* Licence: 'GPLv3'
* Project URL : https://git.ias.u-psud.fr/medoc/PySitools2

# Stakeholders

* Client(s): MEDOC administrators
* User(s): any user
* Project manager: Eric Buchlin, Stephane Caminade
* Project architect(s): Nima Traore
* Project developer(s): Nima Traore

# Software development life cycle and environment

* Life-cycle: Agile
* Source control management: git
* Branching policy : master based on issues
* Versioning policy : Major/Minor
* Language(s): Python
* External dependencies control: python requirements
* Building/packaging: NA
* Type of tests available: unit tests/integration tests
* Unit test framework: PyTest
* Validation plan: NA
* Documentation: "README.md"
* Issues management: Airtable issues


